import { useEffect } from "react"
import { useDispatch, useSelector } from "react-redux"
import { myFriends } from "../../store/user/action"
import { unfriend } from "../../store/user/action"
import { Link } from "react-router-dom"
import "./friends.scss"

export const MyFriends=()=>{
    const dispatch=useDispatch()
    useEffect(()=>{
        dispatch(myFriends())
    }, [])
    const a=useSelector(state=>state.us.users);
    console.log(a)
    return(
        <div>
            <div className="mneu">
                <div>
                    <Link to="/profile">PROFILE</Link>
                    <Link to="/showmyrequests">REQUEST</Link>
                    <Link to="/showmyfriends">FRIENDS</Link>
                    <Link to="/posts">POSTS</Link>
                    <Link to="/settings">Settings</Link>
                    <Link to="/">Logout</Link>
                </div>
            </div>
            {a?.map((e,i)=>{
                return(
                    <div key={i}  className="friendName">
                        <span>{e.name} {e.surname}</span>
                        <button onClick={()=>{
                            dispatch(unfriend(e.id))
                        }}>Delete</button>
                    </div>
                )
            })}
        </div>
    )
}