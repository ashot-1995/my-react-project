import {showGuest} from "../../store/user/action"
import { useDispatch,useSelector } from "react-redux"
import { useEffect } from "react"
import "./guest.scss"
import { Link } from "react-router-dom"

export const Guest=()=>{
    const dispatch=useDispatch()
    useEffect(async()=>{
        dispatch(showGuest())
    },[])
    const a=useSelector(state=>state.us.guest);
    console.log(a)
    return(
        <div>
            <div>
                <div>
                    <Link to="/profile">PROFILE</Link>
                    <Link to="/showmyrequests">REQUEST</Link>
                    <Link to="/showmyfriends">FRIENDS</Link>
                    <Link to="/posts">POSTS</Link>
                    <Link to="/settings">Settings</Link>
                    <Link to="/">Log Out</Link>
                </div>
            </div>
            <p>{a.name} {a.surname}</p>
        </div>
    )
}