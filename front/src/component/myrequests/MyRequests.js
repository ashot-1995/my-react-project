import { useDispatch,useSelector } from "react-redux";
import { useEffect } from "react";
import {getMyRequests} from "../../store/user/action"
import { acceptRequest } from "../../store/user/action";
import { declineRequest } from "../../store/user/action";
import { canselRequest } from "../../store/user/action";
import { Link } from "react-router-dom";

export const MyRequests=()=>{
    const dispatch=useDispatch();
    useEffect(()=>{
        dispatch(getMyRequests())
    },[])
    const a=useSelector(state=>state.us.users)
    console.log(a)
    return(
        <div>
            <div className="mneu">
                <div>
                    <Link to="/profile">PROFILE</Link>
                    <Link to="/showmyrequests">REQUEST</Link>
                    <Link to="/showmyfriends">FRIENDS</Link>
                    <Link to="/posts">POSTS</Link>
                    <Link to="/settings">Settings</Link>
                    <Link to="/">Logout</Link>
                </div>
            </div>
            {a?.map((e,i)=>{
                return(
                    <div key={i} style={{border:"1px solid black"}}>
                        
                        <p>{e.name} {e.surname}</p>
                        <div>
                            <button onClick={()=>{
                                dispatch(acceptRequest(e.id))
                            }}>Accept</button>
                            <button onClick={()=>{
                                dispatch(declineRequest(e.id))
                            }}>Decline</button>
                        </div>
                    </div>
                )
            })}
        </div>
    )
}