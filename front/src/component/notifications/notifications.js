import { useDispatch } from "react-redux";
import { useSelector } from "react-redux";
import { useEffect } from "react";
import { Link } from "react-router-dom";
import "./notifications.scss"


return(
  <div>
    <div>
        <Link to="/profile">PROFILE</Link>
        <Link to="/showmyrequests">REQUEST</Link>
        <Link to="/showmyfriends">FRIENDS</Link>
        <Link to="/posts">POSTS</Link>
        <Link to="/settings">Settings</Link>
        <Link to="/">Logout</Link>
    </div>
  </div>
)